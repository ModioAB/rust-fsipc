# fsipc

The name comes from a legacy name and is more or less inconsequential these
days, but legacy dies hard.

They are zbus / dbus bindings for the modio-logger projects DBus API, with a
few modifications for usability.

Unless you need to interact with the modio logger API over dbus, you won't need
this library.
