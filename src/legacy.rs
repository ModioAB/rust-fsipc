// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: MIT
use zbus::{proxy, Result};

mod types;
pub use types::{Measure, Measures};
pub use types::{PreparedPoint, Tid};
pub use types::{Transaction, Transactions};

#[proxy(
    interface = "se.modio.logger.fsipc",
    default_service = "se.modio.logger",
    default_path = "/se/modio/logger"
)]
/// The fsipc trait documents the Legacy modio logger "fsipc" setup.
/// This api is organically grown like a midden heap,
/// and some design choices were made simply to avoid touching
/// some parts of the code.
///
/// As such, the return types and data structures used are sometimes
/// misgiving, and other times totally wrong for a useful situation.
///
/// There aren't any big dragons here, however the rats may gnaw on
/// your bones if you're careless.
trait fsipc {
    /// GetBoxid method
    fn get_boxid(&self) -> Result<String>;

    /// Retrieve method
    fn retrieve(&self, key: &str) -> Result<Measure>;

    /// RetrieveAll method
    fn retrieve_all(&self) -> Result<Measures>;

    /// Store method
    fn store(&self, key: &str, value: &str) -> Result<()>;

    /// StoreWithTime method
    fn store_with_time(&self, key: &str, value: &str, when: u64) -> Result<()>;

    /// Signal passed when new measures are added
    #[zbus(signal)]
    fn store_signal(&self, key: &str, value: &str, when: u64) -> zbus::Result<()>;

    /// Signal passed when a new Transaction is added.
    #[zbus(signal)]
    fn transaction_added(&self, key: &str) -> zbus::Result<()>;

    /// TransactionAdd method
    fn transaction_add(&self, key: &str, expected: &str, target: &str, token: &str) -> Result<()>;

    /// TransactionFail method
    fn transaction_fail(&self, t_id: Tid) -> Result<()>;

    /// TransactionGet method
    fn transaction_get(&self, keyspace: &str) -> Result<Transactions>;

    /// TransactionPass method
    fn transaction_pass(&self, t_id: Tid) -> Result<()>;

    /// ValidKey method
    fn valid_key(&self, key: &str) -> Result<bool>;

    /// Ping method
    /// ( may not be implemented)
    fn ping(&self) -> Result<String>;

    /// Done method
    /// (may not be implemented)
    fn done(&self) -> Result<()>;

    /// Prepare datapoints for delivery
    fn prepare_datapoints(&self, maximum: u32) -> Result<Vec<PreparedPoint>>;

    /// Prepare datapoints for delivery
    fn prepare_modio_datapoints(&self, maximum: u32) -> Result<Vec<PreparedPoint>>;

    /// Remove a batch of prepared datapoints
    fn remove_prepared(&self, items: Vec<i64>) -> Result<()>;
}

#[cfg(test)]
mod tests {
    use super::fsipcProxy;

    #[async_std::test]
    async fn it_works() {
        let conn = zbus::Connection::session().await.unwrap();
        let proxy = fsipcProxy::builder(&conn)
            .destination("se.modio.logger.TestFsIPC")
            .unwrap()
            .path("/se/modio/logger")
            .unwrap()
            .build()
            .await;
        assert!(proxy.is_ok());
    }
}
