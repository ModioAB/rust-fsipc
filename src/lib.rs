// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: MIT
use std::time::SystemTime;

pub mod legacy;
pub mod logger1;

#[must_use]
/// Return current time as u64,  or 0 if it fails for some reason.
pub fn unixtime() -> u64 {
    match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(n) => n.as_secs(),
        Err(_) => 0,
    }
}

#[cfg(test)]
mod tests {
    use crate::legacy::fsipcProxy;
    use crate::logger1::Logger1Proxy;

    #[async_std::test]
    async fn it_works() {
        let conn = zbus::Connection::session().await.unwrap();
        let proxy = fsipcProxy::builder(&conn)
            .destination("se.modio.logger.TestFsIPC")
            .unwrap()
            .path("/se/modio/logger")
            .unwrap()
            .build()
            .await;
        assert!(proxy.is_ok());
    }

    #[async_std::test]
    async fn logger_builds() {
        let conn = zbus::Connection::session().await.unwrap();
        let proxy = Logger1Proxy::builder(&conn)
            .destination("se.modio.logger.TestFsIPC")
            .unwrap()
            .path("/se/modio/logger")
            .unwrap()
            .build()
            .await;
        assert!(proxy.is_ok());
    }
}
